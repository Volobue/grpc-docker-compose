package mongo

import (
	"context"
	"cv-service/internal/core"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type UserRepository struct {
	collection *mongo.Collection
}

func NewUserRepository(collection *mongo.Collection) *UserRepository {
	return &UserRepository{collection: collection}
}

func (repository *UserRepository) GetAll(ctx context.Context) (*[]core.Resume, error) {
	ctxTimeout, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	resumesChannel := make(chan *[]core.Resume)
	var err error

	go func() {
		err = repository.retrieveResumes(ctxTimeout, resumesChannel)
	}()

	if err != nil {
		return nil, err
	}

	var resumes *[]core.Resume

	select {
	case <-ctxTimeout.Done():
		fmt.Println("Processing timeout in mongo")
		break
	case resumes = <-resumesChannel:
		fmt.Println("Finished processing")
	}

	return resumes, nil
}

func (repository *UserRepository) retrieveResumes(ctx context.Context, channel chan<- *[]core.Resume) (err error) {
	var resumes []core.Resume

	cursor, err := repository.collection.Find(ctx, bson.M{})

	if err != nil {
		return err
	}

	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var resume core.Resume
		if err := cursor.Decode(&resume); err != nil {
			return err
		}
		resumes = append(resumes, resume)
	}

	if err := cursor.Err(); err != nil {
		return err
	}

	channel <- &resumes
	return nil
}
