package main

import (
	"context"
	"cv-service/internal/config"
	"cv-service/internal/repositories/mongo"
	"cv-service/internal/services"
	"cv-service/proto"
	"fmt"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"log"
	"net"
)

var (
	host = "localhost"
	port = "5000"
)

func main() {
	ctx := context.Background()

	err := SetUpViper()

	if err != nil {
		log.Fatalf("Error reading uml file: %v", err)
	}

	addr := fmt.Sprintf("%s:%s", host, port)

	lis, err := net.Listen("tcp", addr)

	if err != nil {
		log.Fatalf("Error starting tcp listener: %v", err)
	}

	mongoDatabase, err := config.SetUpMongoDataBase(ctx)

	if err != nil {
		log.Fatalf("Error starting mongo: %v", err)
	}

	resumeRepository := mongo.NewUserRepository(mongoDatabase.Collection("resumes"))
	resumeService := services.NewResumeService(resumeRepository)

	grpcServer := grpc.NewServer()

	proto.RegisterResumeServiceServer(grpcServer, resumeService)

	fmt.Printf("gRPC started at port %v\n", port)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Error starting gRPC %v", err)
	}
}

func SetUpViper() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	return nil
}
