package main

import (
	"context"
	"fmt"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"job-service/internal/config"
	"job-service/internal/repositories/mongo"
	"job-service/internal/services"
	"job-service/proto"
	"log"
	"net"
)

var (
	host = "localhost"
	port = "5001"
)

func main() {
	ctx := context.Background()

	err := SetUpViper()

	if err != nil {
		log.Fatalf("Error reading uml file: %v", err)
	}

	addr := fmt.Sprintf("%s:%s", host, port)

	lis, err := net.Listen("tcp", addr)

	if err != nil {
		log.Fatalf("Error starting tcp listener: %v", err)
	}

	mongoDatabase, err := config.SetUpMongoDataBase(ctx)

	if err != nil {
		log.Fatalf("Error starting mongo: %v", err)
	}

	jobRepository := mongo.NewJobRepository(mongoDatabase.Collection("job"))
	jobService := services.NewJobService(jobRepository)

	grpcServer := grpc.NewServer()

	proto.RegisterJobServiceServer(grpcServer, jobService)

	fmt.Printf("gRPC started at port %v\n", port)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Error starting gRPC %v", err)
	}
}

func SetUpViper() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	return nil
}
