package ru.itis.client.service.impl;

import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import ru.itis.client.grpc.pb.resume.ResumeRequest;
import ru.itis.client.grpc.pb.resume.ResumeResponse;
import ru.itis.client.grpc.pb.resume.ResumeServiceGrpc;
import ru.itis.client.service.ResumeService;

@Service
public class ResumeServiceGrpcClientImpl implements ResumeService {

    @GrpcClient("resume-service")
    private ResumeServiceGrpc.ResumeServiceBlockingStub resumeService;

    @Override
    public ResumeResponse getResumes() {
        return resumeService.getResumes(ResumeRequest.newBuilder()
                .build());
    }
}
