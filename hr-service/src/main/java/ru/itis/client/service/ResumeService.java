package ru.itis.client.service;

import ru.itis.client.grpc.pb.resume.ResumeResponse;

public interface ResumeService {
    ResumeResponse getResumes();
}
