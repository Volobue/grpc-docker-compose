package ru.itis.client.grpc.pb.resume;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.9.1)",
    comments = "Source: resume.proto")
public final class ResumeServiceGrpc {

  private ResumeServiceGrpc() {}

  public static final String SERVICE_NAME = "ResumeService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getGetResumesMethod()} instead. 
  public static final io.grpc.MethodDescriptor<ru.itis.client.grpc.pb.resume.ResumeRequest,
      ru.itis.client.grpc.pb.resume.ResumeResponse> METHOD_GET_RESUMES = getGetResumesMethod();

  private static volatile io.grpc.MethodDescriptor<ru.itis.client.grpc.pb.resume.ResumeRequest,
      ru.itis.client.grpc.pb.resume.ResumeResponse> getGetResumesMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<ru.itis.client.grpc.pb.resume.ResumeRequest,
      ru.itis.client.grpc.pb.resume.ResumeResponse> getGetResumesMethod() {
    io.grpc.MethodDescriptor<ru.itis.client.grpc.pb.resume.ResumeRequest, ru.itis.client.grpc.pb.resume.ResumeResponse> getGetResumesMethod;
    if ((getGetResumesMethod = ResumeServiceGrpc.getGetResumesMethod) == null) {
      synchronized (ResumeServiceGrpc.class) {
        if ((getGetResumesMethod = ResumeServiceGrpc.getGetResumesMethod) == null) {
          ResumeServiceGrpc.getGetResumesMethod = getGetResumesMethod = 
              io.grpc.MethodDescriptor.<ru.itis.client.grpc.pb.resume.ResumeRequest, ru.itis.client.grpc.pb.resume.ResumeResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "ResumeService", "GetResumes"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ru.itis.client.grpc.pb.resume.ResumeRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ru.itis.client.grpc.pb.resume.ResumeResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new ResumeServiceMethodDescriptorSupplier("GetResumes"))
                  .build();
          }
        }
     }
     return getGetResumesMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ResumeServiceStub newStub(io.grpc.Channel channel) {
    return new ResumeServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ResumeServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ResumeServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ResumeServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ResumeServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class ResumeServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getResumes(ru.itis.client.grpc.pb.resume.ResumeRequest request,
        io.grpc.stub.StreamObserver<ru.itis.client.grpc.pb.resume.ResumeResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetResumesMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetResumesMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ru.itis.client.grpc.pb.resume.ResumeRequest,
                ru.itis.client.grpc.pb.resume.ResumeResponse>(
                  this, METHODID_GET_RESUMES)))
          .build();
    }
  }

  /**
   */
  public static final class ResumeServiceStub extends io.grpc.stub.AbstractStub<ResumeServiceStub> {
    private ResumeServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ResumeServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ResumeServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ResumeServiceStub(channel, callOptions);
    }

    /**
     */
    public void getResumes(ru.itis.client.grpc.pb.resume.ResumeRequest request,
        io.grpc.stub.StreamObserver<ru.itis.client.grpc.pb.resume.ResumeResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetResumesMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ResumeServiceBlockingStub extends io.grpc.stub.AbstractStub<ResumeServiceBlockingStub> {
    private ResumeServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ResumeServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ResumeServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ResumeServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ru.itis.client.grpc.pb.resume.ResumeResponse getResumes(ru.itis.client.grpc.pb.resume.ResumeRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetResumesMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ResumeServiceFutureStub extends io.grpc.stub.AbstractStub<ResumeServiceFutureStub> {
    private ResumeServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ResumeServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ResumeServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ResumeServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ru.itis.client.grpc.pb.resume.ResumeResponse> getResumes(
        ru.itis.client.grpc.pb.resume.ResumeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetResumesMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_RESUMES = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ResumeServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ResumeServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_RESUMES:
          serviceImpl.getResumes((ru.itis.client.grpc.pb.resume.ResumeRequest) request,
              (io.grpc.stub.StreamObserver<ru.itis.client.grpc.pb.resume.ResumeResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ResumeServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ResumeServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ru.itis.client.grpc.pb.resume.ResumeOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ResumeService");
    }
  }

  private static final class ResumeServiceFileDescriptorSupplier
      extends ResumeServiceBaseDescriptorSupplier {
    ResumeServiceFileDescriptorSupplier() {}
  }

  private static final class ResumeServiceMethodDescriptorSupplier
      extends ResumeServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ResumeServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ResumeServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ResumeServiceFileDescriptorSupplier())
              .addMethod(getGetResumesMethod())
              .build();
        }
      }
    }
    return result;
  }
}
