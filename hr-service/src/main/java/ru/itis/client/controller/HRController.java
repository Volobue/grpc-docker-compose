package ru.itis.client.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.client.grpc.pb.job.Job;
import ru.itis.client.grpc.pb.resume.Resume;
import ru.itis.client.service.JobService;
import ru.itis.client.service.ResumeService;
import ru.itis.client.wrapper.JobWrapper;
import ru.itis.client.wrapper.ResumeWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class HRController {

    private final ResumeService resumeService;
    private final JobService jobService;

    @GetMapping("/resumes")
    public ResponseEntity<List<ResumeWrapper>> getResumes() {
        List<ResumeWrapper> result = new ArrayList<>();
        List<Resume> resumes = resumeService.getResumes().getResumesList();
        for (Resume resume: resumes) {
            ResumeWrapper wrapper = new ResumeWrapper(resume);
            result.add(wrapper);
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/jobs")
    public ResponseEntity<List<JobWrapper>> getJobs() {
        List<JobWrapper> result = new ArrayList<>();
        List<Job> jobs = jobService.getJobs().getJobsList();
        for (Job job: jobs) {
            JobWrapper wrapper = new JobWrapper(job);
            result.add(wrapper);
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/candidates")
    public ResponseEntity<Map<String,List<ResumeWrapper>>> getCandidates() {

        Map<String,List<ResumeWrapper>> result = new HashMap<>();

        ResponseEntity<List<ResumeWrapper>> resumesResponse = getResumes();
        List<ResumeWrapper> resumes = resumesResponse.getBody();

        ResponseEntity<List<JobWrapper>> jobsResponse = getJobs();
        List<JobWrapper> jobs = jobsResponse.getBody();

        for(JobWrapper job: jobs) {
            List<ResumeWrapper> resumesForJob = new ArrayList<>();
            for (ResumeWrapper resume: resumes) {
                if (job.getAgeFrom() <= resume.getAge() && job.getExperience() <= resume.getExperience()) {
                    resumesForJob.add(resume);
                }
            }
            result.put(job.getName(),resumesForJob);
        }

        return ResponseEntity.ok(result);
    }
}
