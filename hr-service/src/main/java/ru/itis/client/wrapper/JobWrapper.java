package ru.itis.client.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.itis.client.grpc.pb.job.Job;

public class JobWrapper {
    private Job job;

    public JobWrapper(Job job) {
        this.job = job;
    }

    public String getId() {
        return job.getId();
    }

    public String getName() {
        return job.getName();
    }

    public String getStack() {
        return job.getStack();
    }

    public int getAgeFrom() {
        return job.getAgeFrom();
    }

    public int getExperience() {
        return job.getExperience();
    }

    public String getLanguage() {
        return job.getLanguage();
    }

    public String getProgrammingLanguage() {
        return job.getProgrammingLanguage();
    }

    public int getSalaryFrom() {
        return job.getSalaryFrom();
    }

    public String getLocation() {
        return job.getLocation();
    }

    public boolean getRelocation() {
        return job.getRelocation();
    }

    public String getGenderRequired() {
        return job.getGenderRequired();
    }

    public boolean getInsurance() {
        return job.getInsurance();
    }

    @JsonIgnore
    public com.google.protobuf.UnknownFieldSet getUnknownFields() {
        return job.getUnknownFields();
    }
}
