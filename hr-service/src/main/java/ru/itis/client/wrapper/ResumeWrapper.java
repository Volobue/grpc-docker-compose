package ru.itis.client.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.itis.client.grpc.pb.resume.Resume;

public class ResumeWrapper {

    private Resume resume;

    public ResumeWrapper(Resume resume) {
        this.resume = resume;
    }

    public String getId() {
        return resume.getId();
    }

    public String getFirstName() {
        return resume.getFirstName();
    }

    public String getLastName() {
        return resume.getLastName();
    }

    public int getAge() {
        return resume.getAge();
    }

    public int getExperience() {
        return resume.getExperience();
    }

    public String getMainLanguage() {
        return resume.getMainLanguage();
    }

    public String getSecondaryLanguage() {
        return resume.getSecondaryLanguage();
    }

    public String getProgrammingLanguage() {
        return resume.getProgrammingLanguage();
    }

    public int getSalaryFrom() {
        return resume.getSalaryFrom();
    }

    public String getLocation() {
        return resume.getLocation();
    }

    public boolean isWillingToMoveOut() {
        return resume.getWillingToMoveOut();
    }

    public String getGender() {
        return resume.getGender();
    }

    @JsonIgnore
    public com.google.protobuf.UnknownFieldSet getUnknownFields() {
        return resume.getUnknownFields();
    }
}

